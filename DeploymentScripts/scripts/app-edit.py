#---------------------------------------------------------------------
# Nombre: app-edit.py
# Descripcion: Script generico de modificación de configuraciones de 
#              aplicaciones (EAR) 
#
# History
#   fecha     ver  aut     modificacion 
# ----------  ---  ------  -------------------------------------------
# 27/05/2016  0.1  devm    version inicial
# 30/05/2016  0.2  devm    correcciones
# 24/06/2016  0.3  devm    Adecuciones sysexit
#
# Autores
# --------------------------------------------------------------------
# devm -> diego_mendoza@microgestion.com
#---------------------------------------------------------------------
import sys, time, getopt, os.path;
import AdminUtilities;
version = "Version 0.3 2016.06.24"

#---------------------------------------------------------------------
# Nombre: actionEditCheck
# Descripcion: Comprueba que los parámetros suministrados sean válidos
#              para realizar la configuración de la aplicacion
#---------------------------------------------------------------------
def actionEditCheck(appName, editionName, sessMgrFile) :
  msgSessMgrFileNotFound   = "No se pudo encontrar el archivo de configuración de Session Manager en la ruta especificada '%(sessMgrFile)s'.";
  #msgErrClusterWrongState = "El cluster especificado '%(clusterName)s' no esta en un estado adecuado."
  #msgErrClusterNotFound   = "El cluster especificado '%(clusterName)s' no existe."
  #msgClusterState         = "El cluster especificado '%(clusterName)s' se encuentra en el estado '%(clusterState)s'."

  #--------------------------------------------------------------------
  # Verificaciones de existencia de la aplicación
  #--------------------------------------------------------------------
  appNameEdition = appName
  if(editionName) :
    appNameEdition = appName + "-edition" + editionName

  appExist = AdminConfig.getid("/Deployment:"+appNameEdition+"/" )
  if (len(appExist) == 0):
    raise NoFatalWebSphereException("La aplicacion especificada '%(appNameEdition)s' NO existe." % locals())

  #--------------------------------------------------------------------
  # Verificaciones de existencia de archivos
  #--------------------------------------------------------------------
  if (sessMgrFile):
    fileExist = os.path.isfile(sessMgrFile);
    if (not fileExist):
      raise NoFatalWebSphereException( msgSessMgrFileNotFound % locals())

#---------------------------------------------------------------------
# Nombre: actionEdit
# Descripcion: Edita la configuración de una aplicación
#---------------------------------------------------------------------
def actionEdit( cmdName, appName, editionName, jsfImpl, clModeApp, clModeWMod):
  msgAppEditSummary = "%(cmdName)s: La aplicacion %(appNameEdition)s se ha modificado sin errores"
  

  try:
    global AdminApp 
    cell = AdminConfig.list("Cell")
    cellName = AdminConfig.showAttribute(cell, "name")
    #--------------------------------------------------------------------
    # Información sobre la ejecución de actionEdit
    #--------------------------------------------------------------------
    print "---------------------------------------------------------------"
    print " actionEdit "
    print "---------------------------------------------------------------"
    print " Nombre de la aplicacion:       "+appName
    if (editionName):
      print " Nombre de la edicion:          "+editionName    

    if (jsfImpl):
      print " Implementacion JSF:            "+jsfImpl 
    if (clModeApp):
      print " ClassLoader a nivel de app:    "+clModeApp
    if (clModeWMod):
      print " ClassLoader a nivel de modulo: "+clModeWMod
    print "---------------------------------------------------------------"
    #--------------------------------------------------------------------
    # Ejecución de actionEdit
    #--------------------------------------------------------------------
    appNameEdition = appName
    if(editionName) :
      appNameEdition = appName + "-edition" + editionName    
    
    # Obtener referencia al despliegue de la aplicacion
    deployment = AdminConfig.getid("/Deployment:"+appNameEdition+"/") 
    deploymentObject = AdminConfig.showAttribute(deployment, "deployedObject")
    
    #--------------------------------------------------------------------
    # Configuracion de  de actionEdit
    #--------------------------------------------------------------------    
    if (jsfImpl):
      AdminTask.modifyJSFImplementation(appNameEdition, '[-implName "'+jsfImpl+'"]')
     
    #--------------------------------------------------------------------
    # Configuracion de  de actionEdit
    #--------------------------------------------------------------------    
    if (clModeApp):
      # A nivel de aplicacion
      classloader = AdminConfig.showAttribute(deploymentObject, "classloader")
      AdminConfig.modify(classloader, [["mode", clModeApp]] )
    if (clModeWMod):
      # A nivel de modulo
      modules = AdminConfig.showAttribute(deploymentObject, 'modules')
      modules = modules[1:len(modules)-1].split(" ")
      for module in modules:
        if (module.find('WebModuleDeployment')!= -1):
          AdminConfig.modify(module, [['classloaderMode', 'PARENT_LAST']])     
    AdminConfig.save()   
    print msgAppEditSummary % locals()
      
  except:
    typ, val, tb = sys.exc_info()
    if (typ==SystemExit):  raise SystemExit, val
    val = "%s %s" % (sys.exc_type, sys.exc_value)
    raise NoFatalWebSphereException (val)
    return -1
    
  return 1  # succeed

#---------------------------------------------------------------------
# Nombre: processEditActions
# Descripción: Determina y ejecuta las tareas necesarias para reaizar  
#              la configuración de una aplicacion
#---------------------------------------------------------------------
def processEditActions(Opts, cmdName) :
  msgOk = "\n[OK] %(cmdName)s ha modificado la aplicacion %(appNameEdition)s."
  scsErr = "%(cmdName)s: Error durante las tareas de configuracion: %(excepcion)s"
  exeErr = "%s: Error durante las tareas de configuracion: %s %s %s"
  #-------------------------------------------------------------------
  # Asigna los valores a variables a partir de los valores del 
  # diccionario de opciones
  #-------------------------------------------------------------------
  for key in Opts.keys() :
    val = Opts[ key ];
    cmd = '%s=Opts["%s"]' % ( key, key );
    exec( cmd );

  try :
    appNameEdition = appName
    if(editionName) :
      appNameEdition = appName + "-edition" + editionName
    
    actionEditCheck(appName, editionName, sessMgrFile)
    actionEdit(cmdName, appName, editionName, jsfImpl, clModeApp, clModeWMod)
    
    print msgOk % locals()
  except NoFatalWebSphereException, excepcion :
    print scsErr % locals();
    sys.exit(99);
  except :
    exc_type, exc_obj, exc_tb = sys.exc_info()
    print exeErr % (cmdName, sys.exc_type, sys.exc_value, exc_tb.tb_lineno)
    sys.exit(99);

#---------------------------------------------------------------------
# Nombre: parseOptions
# Descripción: Procesa los argumentos pasados por el usuario
#---------------------------------------------------------------------
def parseOptions( cmdName ) :
  shortForm = 'sf:js:ca:cw:an:en:wt:';
  longForm  = 'sessMgrFile=,jsfImpl=,clModeApp=,clModeWMod=,appName=,editionName=,waitingTime='.split( ',' );
  optErr    = '%s: Se ha verificado un error durante el procesamiento: %s\n';

  try :
    opts, args = getopt.getopt( sys.argv, shortForm, longForm );
  except getopt.GetoptError:
    print optErr % (cmdName, sys.exc_value)
    showUsage( cmdName );
  #-------------------------------------------------------------------
  # Creación del diccionario Opts con las claves definidas en longForm
  #-------------------------------------------------------------------
  Opts = {};
  for name in longForm :
    if name[ -1 ] == '=' :
      name = name[ :-1 ]
    Opts[ name ] = None;
        
  #-------------------------------------------------------------------
  # Seteo de valores por omision
  #-------------------------------------------------------------------
  Opts[ 'waitingTime' ] = 300

  #-------------------------------------------------------------------
  # Procesa la lista de opciones retornadas por getopt()
  #-------------------------------------------------------------------
  for opt, val in opts :
    if opt in ( '-an', '--appName' )                  : Opts[ 'appName' ] = val
    elif opt in ( '-en', '--editionName' )            : Opts[ 'editionName' ] = val
    elif opt in ( '-sf', '--sessMgrFile' )            : Opts[ 'sessMgrFile' ] = val
    elif opt in ( '-js', '--jsfImpl' )                : Opts[ 'jsfImpl' ] = val
    elif opt in ( '-ca', '--clModeApp' )              : Opts[ 'clModeApp' ] = val
    elif opt in ( '-cw', '--clModeWMod' )             : Opts[ 'clModeWMod' ] = val
    elif opt in ( '-wt', '--waitingTime' )            : Opts[ 'waitingTime' ] = int(val)

  #-------------------------------------------------------------------
  # Validar relaciones entre parámetros
  #-------------------------------------------------------------------
  if (not Opts[ 'appName' ]):
    print '%(cmdName)s: Error en los parametros, se debe especificar el nombre de la aplicación mediante la opcion --appName' % locals() 
    showUsage( cmdName )
  if (Opts[ 'jsfImpl' ] and not (Opts[ 'jsfImpl' ] in ( 'SunRI1.2', 'MyFaces', 'MyFaces2.0' ))):
    print '%(cmdName)s: Error en los parametros, solo se pueden especificar las implementaciones SunRI1.2, MyFaces, o MyFaces2.0 en el parametro --jsfImpl ' % locals() 
    showUsage( cmdName )
  if (Opts[ 'clModeApp' ] and not (Opts[ 'clModeApp' ] in ( 'PARENT_LAST', 'PARENT_FIRST' ))):
    print '%(cmdName)s: Error en los parametros, solo se pueden especificar los modos  PARENT_LAST y PARENT_FIRST en los parametros --clModeApp y --clModeWMod ' % locals() 
    showUsage( cmdName )
  if (Opts[ 'clModeWMod' ] and not (Opts[ 'clModeWMod' ] in ( 'PARENT_LAST', 'PARENT_FIRST' ))):
    print '%(cmdName)s: Error en los parametros, solo se pueden especificar los modos  PARENT_LAST y PARENT_FIRST en los parametros --clModeApp y --clModeWMod ' % locals() 
    showUsage( cmdName )
  if ( (not Opts[ 'sessMgrFile' ]) and (not Opts[ 'jsfImpl' ]) and (not Opts[ 'clModeApp' ]) and (not Opts[ 'clModeWMod' ]) ):
    print '%(cmdName)s: Error en los parametros, no se han especificado las configuraciones que se desean modificar' % locals() 
    showUsage( cmdName )
    
  #-------------------------------------------------------------------
  # Retorna el diccionario con los argumentos especificados por el usuario
  #-------------------------------------------------------------------
  return Opts;
  
#---------------------------------------------------------------------
# Nombre: showUsage()
# Descripción: Rutina utilizada para imprimir en la consola el modo de 
#              utilización del script.
#---------------------------------------------------------------------
def showUsage( cmdName = None ) :
  if not cmdName :
    cmdName = 'app-edit.py'

  print '''
-----------------------------------------------------------------------------
Nombre: app-edit.py
Descripcion: Script generico para modificar aplicaciones (EAR)
-----------------------------------------------------------------------------
Utilizacion: app-edit.py [opciones]

Opciones soportadas:

 -an | --appName     <valor> = Nombre de la aplicacion
 -en | --editionName <valor> = Nombre de la edicion asociada a la acplicacion
 -sf | --sessMgrFile <valor> = Ruta completa al archivo donde se encuentran
                               las propiedades de configuracion de la sesion
 -js | --jsfImpl     <valor> = Nombre de la implementación de Java Server
                               Faces, los valores soportados son SunRI1.2,
                               MyFaces, o MyFaces2.0
 -ca | --clModeApp   <valor> = Nombre del modo a especificar en la  
                               configuraciónde cargadores de clase a nivel 
                               de aplicacion, los valores soportados son
                               PARENT_LAST y PARENT_FIRST
 -cw | --clModeWMod  <valor> = Nombre del modo a especificar en la 
                               configuración de cargadores de clase a nivel 
                               de modulo Web, los valores soportados son 
                               PARENT_LAST y PARENT_FIRST
 -wt | --waitingTime <valor> = Valor en segundos correspondientes al tiempo
                               maximo de espera, de los procesos asincronicos
                               despues de este tiempo se lanzara un error y
                               se debera verifica el resultado de la
                               ejecucion manualmente

Ejemplos:
  wsadmin.sh -f app-edit.py --appName MyApp01 --clModeApp  
  wsadmin.sh -f app-edit.py --appName MyApp01 --editionName v01 --clModeWMod 
    
  \n''' % locals();
  sys.exit(99);

#---------------------------------------------------------------------
# Clase: NoFatalWebSphereException(Exception)
# Descripción: Clase utilizada para el lanzamiento de exepciones de
#              situaciones atrapadas en el código.
#---------------------------------------------------------------------
class NoFatalWebSphereException(Exception):
  pass
  
#---------------------------------------------------------------------
# Este es el punto de entrada de la ejecución del script
#---------------------------------------------------------------------
if ( __name__ == '__main__' ) :
  cmdName = 'app-edit.py';
  print '%(cmdName)s: iniciando la ejecucion del script %(version)s' % locals()
  Opts = parseOptions( cmdName );
  processEditActions(Opts, cmdName);
else :
  print 'Error: este script debe ejecutarse, no importarse.\n';
  showUsage();
