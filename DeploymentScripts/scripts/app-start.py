#---------------------------------------------------------------------
# Nombre: app-start.py.py
# Descripcion: Script generico para iniciar aplicaciones (EAR) 
#
# History
#   fecha     ver  aut     modificacion 
# ----------  ---  ------  -------------------------------------------
# 06/05/2016  0.1  devm    version inicial utilizando getopt
# 17/05/2016  0.2  devm    se considera websphere.cluster.partial.*
# 17/05/2016  0.3  devm    inicio en servidor stand alone
# 24/06/2016  0.4  devm    Adecuciones sysexit
#
# Autores
# --------------------------------------------------------------------
# devm -> diego_mendoza@microgestion.com
#---------------------------------------------------------------------
import sys, getopt, os.path;
import AdminUtilities;
version = "Version 0.4 2016.06.24"

#---------------------------------------------------------------------
# Nombre: actionStartCheck
# Descripcion: Comprueba que los par�metros suministrados sean v�lidos
#              para realizar el inicio de la aplicacion
#---------------------------------------------------------------------
def actionStartCheck(appName, editionName, clusterName) :
  msgErrClusterWrongState = "El cluster especificado '%(clusterName)s' no esta en un estado adecuado."
  msgErrClusterNotFound   = "El cluster especificado '%(clusterName)s' no existe."
  msgErrClusterNotRunning = "El cluster especificado '%(clusterName)s' no esta en ejecucion."
  msgClusterState         = "El cluster especificado '%(clusterName)s' se encuentra en el estado '%(clusterState)s'."
  msgErrAppNotExist       = "La aplicacion especificada '%(appNameEdition)s' NO existe."
  msgErrAppNotInCluster   = "La aplicacion especificada '%(appNameEdition)s' NO se encuentra desplegada en el cluster '%(clusterName)s'."
  msgErrAppStarted        = "La aplicacion especificada '%(appName)s' se encuentra iniciada."
  
  #--------------------------------------------------------------------
  # Verificaciones para frenado en cluster
  #--------------------------------------------------------------------
  if (clusterName):
    clusterExist = AdminConfig.getid("/ServerCluster:"+clusterName+"/")
    if (len(clusterExist) == 0):
      raise NoFatalWebSphereException( msgErrClusterNotFound % locals() )
      
    clusterMbean = AdminControl.completeObjectName("type=Cluster,name="+clusterName+",*")
    if (len(clusterMbean) == 0):
      raise NoFatalWebSphereException( msgErrClusterNotRunning % locals() )
    clusterState = AdminControl.getAttribute(clusterMbean, "state")
    print msgClusterState % locals()
    if (clusterState != "websphere.cluster.running"):
      if (clusterState != "websphere.cluster.partial.start"):
        if (clusterState != "websphere.cluster.partial.stop"):
          raise NoFatalWebSphereException( msgErrClusterWrongState % locals() )

  appNameEdition = appName
  if(editionName) :
    appNameEdition = appName + "-edition" + editionName

  #--------------------------------------------------------------------
  # Verificaciones de existencia de la aplicaci�n
  #--------------------------------------------------------------------
  appExist = AdminConfig.getid("/Deployment:"+appNameEdition+"/" )
  if (len(appExist) == 0):
    raise NoFatalWebSphereException( msgErrAppNotExist % locals() )

  #--------------------------------------------------------------------
  # Verificaci�n de que la app se encuentre desplegada en el cluster
  #--------------------------------------------------------------------
  if (clusterName):  
    targets = AdminConfig.showAttribute(appExist, "deploymentTargets")
    targets = AdminUtilities.convertToList(targets)
    found = 0
    for dt in targets:
      if (dt.find("ClusteredTarget") > 0):
        cName = AdminConfig.showAttribute(dt, "name")
        if (cName ==  clusterName):
          found = 1
    if (found == 0):
      raise NoFatalWebSphereException( msgErrAppNotInCluster % locals() )

  #--------------------------------------------------------------------
  # verificar que la aplicaci�n no se encuentre en ejecuci�n
  runningApp = AdminControl.queryNames("type=Application,name="+appName+",*")
  #--------------------------------------------------------------------
  if (len(runningApp) > 0):
    print msgErrAppStarted % locals()

#---------------------------------------------------------------------
# Nombre: actionStart
# Descripcion: Inicia una aplicaci�n
#---------------------------------------------------------------------
def actionStart( cmdName, appName, editionName, clusterName, nodeName, serverName):
  msgStop           = "%(cmdName)s: Frenando la aplicacion %(appName)s en el servidor %(serverName)s del nodo %(nodeName)s."
  msgStart          = "%(cmdName)s: Iniciando la aplicacion %(appNameEdition)s en el servidor %(serverName)s del nodo %(nodeName)s.\n"
  appMngrErr        = "%(cmdName)s: El Application Manager no se encuentra en ejecucion en el servidor %(serverName)s."
  msgServerStatus   = "Estado del servidor %(serverName)s ubicado en el nodo %(nodeName)s --> %(serverStatus)s"
  msgServerSkiped   = "%(cmdName)s: No se iniciara la aplicacion en el servidor %(serverName)s del nodo %(nodeName)s."
  msgAppInitSryClus = "%(cmdName)s: La aplicacion %(appNameEdition)s se ha iniciado en %(iMemberStartedCount)i de %(iMemberCount)i miembros del cluster '%(clusterName)s'."
  msgAppInitSrySrv  = "%(cmdName)s: La aplicacion %(appNameEdition)s se ha iniciado en el servidor '%(serverName)s' del nodo %(nodeName)s."
  msgAppInitErrClus = "%(cmdName)s: La aplicacion %(appNameEdition)s no se iniciado en ninguno de los %(iMemberCount)i miembros del cluster '%(clusterName)s'."
  msgAppInitErrSrv  = "%(cmdName)s: La aplicacion %(appNameEdition)s no se ha iniciado en el servidor '%(serverName)s' del nodo %(nodeName)s."
  iMemberStartedCount = 0
  try:
    global AdminApp 
    cell = AdminConfig.list("Cell")
    cellName = AdminConfig.showAttribute(cell, "name")
    #--------------------------------------------------------------------
    # Informaci�n sobre la ejecuci�n de actionStart
    #--------------------------------------------------------------------
    print "---------------------------------------------------------------"
    print " actionStart "
    print "---------------------------------------------------------------"
    print " Nombre de la aplicacion:     "+appName
    if (editionName):
      print " Nombre de la edicion:        "+editionName    
    if (clusterName):
      print " Nombre del cluster:          "+clusterName
    else:
      print " Nombre del nodo:             "+nodeName
      print " Nombre del server:           "+serverName
    print "---------------------------------------------------------------"
    #--------------------------------------------------------------------
    # Ejecuci�n de actionstart
    #--------------------------------------------------------------------
    appNameEdition = appName
    if(editionName) :
      appNameEdition = appName + "-edition" + editionName    

    if (clusterName) :
    #--------------------------------------------------------------------
    # Inicio en cluster
    #--------------------------------------------------------------------    	
      members = AdminConfig.getid("/ServerCluster:"+clusterName+"/ClusterMember:/")
      members = AdminUtilities.convertToList(members)
      
      #--------------------------------------------------------------------
      # Imprimit el estado de cada servidor
      #--------------------------------------------------------------------
      for member in members:
        serverName = AdminConfig.showAttribute(member, "memberName")
        nodeName = AdminConfig.showAttribute(member, "nodeName")
        appServer = AdminControl.queryNames("node="+nodeName+",process="+serverName+",type=Server,*")
        serverStatus = "STOPED"
        
        if (len(appServer) != 0):
          serverStatus = AdminControl.getAttribute(appServer, 'state')
        print msgServerStatus % locals()
      print "---------------------------------------------------------------"

      #--------------------------------------------------------------------
      # Reinicio de la aplicaci�n en cada servidor iniciado del cluster
      #--------------------------------------------------------------------
      iMemberCount = 0
      for member in members:
        iMemberCount += 1
        serverName = AdminConfig.showAttribute(member, "memberName")
        nodeName = AdminConfig.showAttribute(member, "nodeName")
        # Identificaci�n del Applicaation Manager (MBeans)  
        appManager = AdminControl.queryNames("node="+nodeName+",process="+serverName+",type=ApplicationManager,*")
        if (len(appManager) == 0):
          print msgServerSkiped % locals()
          continue
        else:
          appManager = AdminUtilities.convertToList(appManager)

        # Recorrer cada "App Manager" e iniciar la aplicaci�n
        for mgr in appManager:
          AdminUtilities.infoNotice("Iniciando la aplicacion ...")
          # Si la aplicaci�n no se encuentre en ejecuci�n se frena
          runningApp = AdminControl.queryNames("type=Application,name="+appName+",node="+nodeName+",process="+serverName+",*")
          if (len(runningApp) > 0):
            print msgStop % locals()
            AdminControl.invoke(mgr, 'stopApplication', appName)
          print msgStart % locals()
          AdminControl.invoke(mgr, "startApplication", appNameEdition)
          iMemberStartedCount+=1
    else :
    #--------------------------------------------------------------------
    # Inicio en servidor
    #--------------------------------------------------------------------    
      # Identificaci�n del Applicaation Manager (MBeans)  
      appManager = AdminControl.queryNames("node="+nodeName+",process="+serverName+",type=ApplicationManager,*")
      if (len(appManager) == 0):
        print msgServerSkiped % locals()
      else:
        appManager = AdminUtilities.convertToList(appManager)

      # Recorrer cada "App Manager" e iniciar la aplicaci�n
      for mgr in appManager:
        AdminUtilities.infoNotice("Iniciando la aplicacion ...")
        # Si la aplicaci�n se encuentre en ejecuci�n se frena
        runningApp = AdminControl.queryNames("type=Application,name="+appName+",node="+nodeName+",process="+serverName+",*")
        if (len(runningApp) > 0):
          print msgStop % locals()
          AdminControl.invoke(mgr, 'stopApplication', appName)
        print msgStart % locals()
        AdminControl.invoke(mgr, "startApplication", appNameEdition)
        iMemberStartedCount+=1
    #--------------------------------------------------------------------
    # Resultado
    #--------------------------------------------------------------------    
    if (iMemberStartedCount==0):
      if (clusterName):
        raise Exception(msgAppInitErrClus % locals())
      else:
        raise Exception(msgAppInitErrSrv % locals())
    else:    
      if (clusterName):
        print msgAppInitSryClus % locals()
      else:
        print msgAppInitSrySrv % locals()
           
  except:
    typ, val, tb = sys.exc_info()
    if (typ==SystemExit):  raise SystemExit, val
    val = "%s %s" % (sys.exc_type, sys.exc_value)
    raise NoFatalWebSphereException (val)
    return -1
    
  return 1  # succeed

#---------------------------------------------------------------------
# Nombre: processstartActions
# Descripci�n: Determina y ejecuta las tareas necesarias para reaizar  
#              el inicio de una aplicacion
#---------------------------------------------------------------------
def processStartActions(Opts, cmdName) :
  msgOk = "\n[OK] %(cmdName)s ha iniciado la aplicacion %(appNameEdition)s."
  scsErr = "%(cmdName)s: Error durante las tareas de incio: %(excepcion)s"
  exeErr = "%s: Error durante las tareas de inicio: %s %s %s"
  #-------------------------------------------------------------------
  # Asigna los valores a variables a partir de los valores del 
  # diccionario de opciones
  #-------------------------------------------------------------------
  for key in Opts.keys() :
    val = Opts[ key ];
    cmd = '%s=Opts["%s"]' % ( key, key );
    exec( cmd );

  try :
    appNameEdition = appName
    if(editionName) :
      appNameEdition = appName + "-edition" + editionName
    
    actionStartCheck(appName, editionName, clusterName)
    actionStart(cmdName, appName, editionName, clusterName, nodeName, serverName)
    
    print msgOk % locals()
  except NoFatalWebSphereException, excepcion :
    print scsErr % locals();
    sys.exit(99);
  except :
    exc_type, exc_obj, exc_tb = sys.exc_info()
    print exeErr % (cmdName, sys.exc_type, sys.exc_value, exc_tb.tb_lineno)
    sys.exit(99);

#---------------------------------------------------------------------
# Nombre: parseOptions
# Descripci�n: Procesa los argumentos pasados por el usuario
#---------------------------------------------------------------------
def parseOptions( cmdName ) :
  shortForm = 'ct:cn:nn:sn:an:en:wt:';
  longForm  = 'clusterType=,clusterName=,nodeName=,serverName=,appName=,editionName=,waitingTime='.split( ',' );
  optErr    = '%s: Se ha verificado un error durante el procesamiento: %s\n';

  try :
    opts, args = getopt.getopt( sys.argv, shortForm, longForm );
  except getopt.GetoptError:
    print optErr % (cmdName, sys.exc_value)
    showUsage( cmdName );
  #-------------------------------------------------------------------
  # Creaci�n del diccionario Opts con las claves definidas en longForm
  #-------------------------------------------------------------------
  Opts = {};
  for name in longForm :
    if name[ -1 ] == '=' :
      name = name[ :-1 ]
    Opts[ name ] = None;
        
  #-------------------------------------------------------------------
  # Seteo de valores por omision
  #-------------------------------------------------------------------
  Opts[ 'clusterType' ] = 'Dynamic'
  Opts[ 'waitingTime' ] = 300

  #-------------------------------------------------------------------
  # Procesa la lista de opciones retornadas por getopt()
  #-------------------------------------------------------------------
  for opt, val in opts :
    if opt in   ( '-ct', '--clusterType' )            : Opts[ 'clusterType' ] = val
    elif opt in ( '-cn', '--clusterName' )            : Opts[ 'clusterName' ] = val
    elif opt in ( '-nn', '--nodeName' )               : Opts[ 'nodeName' ] = val
    elif opt in ( '-sn', '--serverName' )             : Opts[ 'serverName' ] = val
    elif opt in ( '-an', '--appName' )                : Opts[ 'appName' ] = val
    elif opt in ( '-en', '--editionName' )            : Opts[ 'editionName' ] = val
    elif opt in ( '-wt', '--waitingTime' )            : Opts[ 'waitingTime' ] = int(val)

  #-------------------------------------------------------------------
  # Validar relaciones entre par�metros
  #-------------------------------------------------------------------
  if (not Opts[ 'appName' ]):
    print '%(cmdName)s: Error en los parametros, se debe especificar el nombre de la aplicaci�n mediante la opcion --appName' % locals() 
    showUsage( cmdName )
  if (Opts[ 'clusterType' ] in ( 'Dynamic', 'Static' )) :
    if (not Opts[ 'clusterName' ]):
      print '%(cmdName)s: Error en los parametros, se debe especificar el nombre del cluster mediante la opcion --clusterName' % locals()
      showUsage( cmdName )
    if (Opts[ 'nodeName' ] or Opts[ 'serverName' ]) :
      print '%(cmdName)s: Error en los parametros, al especificar el inicio en un cluster no se deben especificar las opciones: --serverName, --nodeName' % locals()
      showUsage( cmdName )
  else :
    if (Opts[ 'clusterName' ]):
      print '%(cmdName)s: Error en los parametros, cuando se realiza el inicio fuera de un cluster no se debe especificar la opcion --clusterName' % locals()
      showUsage( cmdName )
  #-------------------------------------------------------------------
  # Retorna el diccionario con los argumentos especificados por el usuario
  #-------------------------------------------------------------------
  return Opts;
  
#---------------------------------------------------------------------
# Nombre: showUsage()
# Descripci�n: Rutina utilizada para imprimir en la consola el modo de 
#              utilizaci�n del script.
#---------------------------------------------------------------------
def showUsage( cmdName = None ) :
  if not cmdName :
    cmdName = 'app-start.py'

  print '''
-----------------------------------------------------------------------------
Nombre: app-start.py
Descripcion: Script generico para iniciar aplicaciones (EAR)
-----------------------------------------------------------------------------
Utilizacion: app-start.py [opciones]

Opciones soportadas:

 -an | --appName     <valor> = Nombre de la aplicacion
 -en | --editionName <valor> = Nombre de la edicion asociada a la acplicacion
 -ef | --earFile     <valor> = Ubicacion completa al archivo .ear
 -ct | --clusterType <valor> = Tipo de despliegue (Dynamic, Static o None),
                               utilizar None para realizar iniciar la
                               aplicacion en un servidor
 -cn | --clusterName <valor> = Nombre del cluster donde sera deplegado el 
                               aplicativo, no aplica si el parametro 
                               --clusterType se establece en None
 -nn | --nodeName    <valor> = Nombre del nodo de despliegue, solo aplicable
                               cuando el parametro --clusterType se establece
                               en None
 -sn | --serverName  <valor> = Nombre del servidor de despliegue, solo
                               aplicable cuando el parametro --clusterType se
                               establece en None 
 -wt | --waitingTime <valor> = Valor en segundos correspondientes al tiempo
                               maximo de espera, de los procesos asincronicos
                               despues de este tiempo se lanzara un error y
                               se debera verifica el resultado de la
                               ejecucion manualmente
Ejemplos:
  wsadmin.sh -f app-start.py --appName MyApp01
  wsadmin.sh -f app-start.py --appName MyApp01 --editionName v01
    
  \n''' % locals();
  sys.exit(99);

#---------------------------------------------------------------------
# Clase: NoFatalWebSphereException(Exception)
# Descripci�n: Clase utilizada para el lanzamiento de exepciones de
#              situaciones atrapadas en el c�digo.
#---------------------------------------------------------------------
class NoFatalWebSphereException(Exception):
  pass

#---------------------------------------------------------------------
# Este es el punto de entrada de la ejecuci�n del script
#---------------------------------------------------------------------
if ( __name__ == '__main__' ) :
  cmdName = 'app-start.py';
  print '%(cmdName)s: iniciando la ejecucion del script %(version)s' % locals()
  Opts = parseOptions( cmdName );
  processStartActions(Opts, cmdName);
else :
  print 'Error: este script debe ejecutarse, no importarse.\n';
  showUsage();