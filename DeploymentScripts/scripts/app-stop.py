#---------------------------------------------------------------------
# Nombre: app-stop.py
# Descripcion: Script generico para frenar aplicaciones (EAR) 
#
# History
#   fecha     ver  aut     modificacion 
# ----------  ---  ------  -------------------------------------------
# 27/05/2016  0.1  devm    version inicial
# 24/06/2016  0.2  devm    Adecuciones sysexit
#
# Autores
# --------------------------------------------------------------------
# devm -> diego_mendoza@microgestion.com
#---------------------------------------------------------------------
import sys, time, getopt, os.path;
import AdminUtilities;
version = "Version 0.2 2016.06.24"

#---------------------------------------------------------------------
# Nombre: actionStopCheck
# Descripcion: Comprueba que los par�metros suministrados sean v�lidos
#              para realizar el frenado de la aplicacion
#---------------------------------------------------------------------
def actionStopCheck(appName, editionName, clusterName) :
  msgErrClusterWrongState = "El cluster especificado '%(clusterName)s' no esta en un estado adecuado."
  msgErrClusterNotFound   = "El cluster especificado '%(clusterName)s' no existe."
  msgClusterState         = "El cluster especificado '%(clusterName)s' se encuentra en el estado '%(clusterState)s'."
  msgErrClusterNotRunning = "El cluster especificado '%(clusterName)s' no esta en ejecucion."
  msgErrAppNotExist       = "La aplicacion especificada '%(appNameEdition)s' NO existe."  
  msgErrAppNotInCluster   = "La aplicacion especificada '%(appNameEdition)s' NO se encuentra desplegada en el cluster '%(clusterName)s'."
  msgErrAppNotStarted     = "La aplicacion especificada '%(appName)s' ya se encuentra frenada."
  #--------------------------------------------------------------------
  # Verificaciones para frenado en cluster
  #--------------------------------------------------------------------
  if (clusterName):
    Opts[ 'stopMode' ] == 'Cluster'
    clusterExist = AdminConfig.getid("/ServerCluster:"+clusterName+"/")
    if (len(clusterExist) == 0):
      raise NoFatalWebSphereException(msgErrClusterNotFound % locals())

    clusterMbean = AdminControl.completeObjectName("type=Cluster,name="+clusterName+",*")
    if (len(clusterMbean) == 0):
      raise NoFatalWebSphereException(msgErrClusterNotRunning % locals())
    clusterState = AdminControl.getAttribute(clusterMbean, "state")
    print msgClusterState % locals()
    if (clusterState != "websphere.cluster.running"):
      if (clusterState != "websphere.cluster.partial.start"):
        if (clusterState != "websphere.cluster.partial.stop"):
          raise NoFatalWebSphereException(msgErrClusterWrongState % locals())
  #--------------------------------------------------------------------
  # Verificaciones de existencia de la aplicaci�n
  #--------------------------------------------------------------------
  appNameEdition = appName
  if(editionName) :
    appNameEdition = appName + "-edition" + editionName

  appExist = AdminConfig.getid("/Deployment:"+appNameEdition+"/" )
  if (len(appExist) == 0):
    raise NoFatalWebSphereException( msgErrAppNotExist % locals())

  #--------------------------------------------------------------------
  # Verificaci�n de que la app se encuentre desplegada en el cluster
  #--------------------------------------------------------------------
  if (clusterName):  
    targets = AdminConfig.showAttribute(appExist, "deploymentTargets")
    targets = AdminUtilities.convertToList(targets)
    found = 0
    for dt in targets:
      if (dt.find("ClusteredTarget") > 0):
        cName = AdminConfig.showAttribute(dt, "name")
        if (cName ==  clusterName):
          found = 1
    if (found == 0):
      raise NoFatalWebSphereException( msgErrAppNotInCluster % locals())

  # verificar que la aplicaci�n no se encuentre en ejecuci�n
  runningApp = AdminControl.queryNames("type=Application,name="+appName+",*")
  if (len(runningApp) == 0):
    print NoFatalWebSphereException( msgErrAppNotStarted % locals() )

#---------------------------------------------------------------------
# Nombre: actionStop
# Descripcion: Frena una aplicaci�n
#---------------------------------------------------------------------
def actionStop( cmdName, appName, editionName, stopMode, clusterName, nodeName, serverName):
  msgStop           = "%(cmdName)s: Frenando la aplicacion %(appName)s en el servidor %(serverName)s del nodo %(nodeName)s."
  msgStoped         = "%(cmdName)s: La aplicacion %(appName)s ya se encuentra frenada en el servidor %(serverName)s del nodo %(nodeName)s."
  appMngrErr        = "%(cmdName)s: El Application Manager no se encuentra en ejecucion en el servidor %(serverName)s."
  msgServerStatus   = "Estado del servidor %(serverName)s ubicado en el nodo %(nodeName)s --> %(serverStatus)s"
  msgServerSkiped   = "%(cmdName)s: No se frenara la aplicacion en el servidor %(serverName)s ubicado en el nodo %(nodeName)s."
  msgAppStopSryClus = "%(cmdName)s: La aplicacion %(appNameEdition)s se ha frenado en %(iMemberStopedCount)i de %(iMemberCount)i miembros del cluster '%(clusterName)s'."
  msgAppStopSrySrv  = "%(cmdName)s: La aplicacion %(appNameEdition)s se ha frenado en el servidor '%(serverName)s' del nodo %(nodeName)s."
  msgAppStopErrClus = "%(cmdName)s: La aplicacion %(appNameEdition)s no se frenado en ninguno de los %(iMemberCount)i miembros del cluster '%(clusterName)s'."
  msgAppStopErrSrv  = "%(cmdName)s: La aplicacion %(appNameEdition)s no se ha frenado en el servidor '%(serverName)s' del nodo %(nodeName)s."
  iMemberStopedCount = 0
  try:
    global AdminApp 
    cell = AdminConfig.list("Cell")
    cellName = AdminConfig.showAttribute(cell, "name")
    #--------------------------------------------------------------------
    # Informaci�n sobre la ejecuci�n de actionStop
    #--------------------------------------------------------------------
    print "---------------------------------------------------------------"
    print " actionStop "
    print "---------------------------------------------------------------"
    print " Nombre de la aplicacion:     "+appName
    if (editionName):
      print " Nombre de la edicion:        "+editionName    
    if (clusterName):
      print " Nombre del cluster:          "+clusterName
    else:
      print " Nombre del nodo:             "+nodeName
      print " Nombre del server:           "+serverName
    print "---------------------------------------------------------------"
    #--------------------------------------------------------------------
    # Ejecuci�n de actionStop
    #--------------------------------------------------------------------
    appNameEdition = appName
    if(editionName) :
      appNameEdition = appName + "-edition" + editionName   
    
    if (stopMode=='Server') : 
    #--------------------------------------------------------------------
    # Frenado en servidor
    #--------------------------------------------------------------------    
      # Identificaci�n del Applicaation Manager (MBeans)  
      appManager = AdminControl.queryNames("node="+nodeName+",process="+serverName+",type=ApplicationManager,*")
      if (len(appManager) == 0):
        print msgServerSkiped % locals()
      else:
        appManager = AdminUtilities.convertToList(appManager)

      # Recorrer cada "App Manager" e iniciar la aplicaci�n
      for mgr in appManager:
        AdminUtilities.infoNotice("Frenando la aplicacion ...")
        # Si la aplicaci�n no se encuentre en ejecuci�n se frena
        runningApp = AdminControl.queryNames("type=Application,name="+appName+",node="+nodeName+",process="+serverName+",*")
        iMemberStopedCount+=1
        if (len(runningApp) > 0):
          print msgStop % locals()
          AdminControl.invoke(mgr, 'stopApplication', appName)
        else:
          print msgStoped % locals()
    
    #--------------------------------------------------------------------
    # Frenado en cluster
    #--------------------------------------------------------------------   
    if (stopMode=='Cluster') : 
      members = AdminConfig.getid("/ServerCluster:"+clusterName+"/ClusterMember:/")
      members = AdminUtilities.convertToList(members)
      #--------------------------------------------------------------------
      # Imprimit el estado de cada servidor
      #--------------------------------------------------------------------
      for member in members:
        serverName = AdminConfig.showAttribute(member, "memberName")
        nodeName = AdminConfig.showAttribute(member, "nodeName")
        appServer = AdminControl.queryNames("node="+nodeName+",process="+serverName+",type=Server,*")
        serverStatus = "STOPED"
      
        if (len(appServer) != 0):
          serverStatus = AdminControl.getAttribute(appServer, 'state')
        print msgServerStatus % locals()
      print "---------------------------------------------------------------"

      #--------------------------------------------------------------------
      # Freno de la aplicaci�n en cada servidor iniciado del cluster
      #--------------------------------------------------------------------
      iMemberCount = 0
      for member in members:
        iMemberCount += 1
        serverName = AdminConfig.showAttribute(member, "memberName")
        nodeName = AdminConfig.showAttribute(member, "nodeName")
        # Identificaci�n del Applicaation Manager (MBeans)  
        appManager = AdminControl.queryNames("node="+nodeName+",process="+serverName+",type=ApplicationManager,*")
        if (len(appManager) == 0):
          print msgServerSkiped % locals()
          continue
        else:
          appManager = AdminUtilities.convertToList(appManager)

        # Recorrer cada "App Manager" e iniciar la aplicaci�n
        for mgr in appManager:
          AdminUtilities.infoNotice("Frenando la aplicacion ...")
          # Si la aplicaci�n no se encuentre en ejecuci�n se frena
          runningApp = AdminControl.queryNames("type=Application,name="+appName+",node="+nodeName+",process="+serverName+",*")
          if (len(runningApp) > 0):
            print msgStop % locals()
            AdminControl.invoke(mgr, 'stopApplication', appName)
          else:
            print msgStoped % locals()
          iMemberStopedCount+=1
          
    #--------------------------------------------------------------------
    # Resultado
    #--------------------------------------------------------------------   
    if (iMemberStopedCount==0):
      if (clusterName):
        raise Exception(msgAppStopErrClus % locals())
      else:
        raise Exception(msgAppStopErrSrv % locals())
    else:    
      if (clusterName):
        print msgAppStopSryClus % locals()
      else:
        print msgAppStopSrySrv % locals()
      
  except:
    typ, val, tb = sys.exc_info()
    if (typ==SystemExit):  raise SystemExit, val
    val = "%s %s" % (sys.exc_type, sys.exc_value)
    raise NoFatalWebSphereException (val)
    return -1
    
  return 1  # succeed
  
#---------------------------------------------------------------------
# Nombre: processStopActions
# Descripci�n: Determina y ejecuta las tareas necesarias para reaizar  
#              el frenado de una aplicacion
#---------------------------------------------------------------------
def processStopActions(Opts, cmdName) :
  msgOk = "\n[OK] %(cmdName)s ha frenado la aplicacion %(appNameEdition)s."
  scsErr = "%(cmdName)s: Error durante las tareas de frenado: %(excepcion)s"
  exeErr = "%s: Error durante las tareas de frenado: %s %s %s"
  #-------------------------------------------------------------------
  # Asigna los valores a variables a partir de los valores del 
  # diccionario de opciones
  #-------------------------------------------------------------------
  for key in Opts.keys() :
    val = Opts[ key ];
    cmd = '%s=Opts["%s"]' % ( key, key );
    exec( cmd );

  try :
    appNameEdition = appName
    if(editionName) :
      appNameEdition = appName + "-edition" + editionName
    
    actionStopCheck(appName, editionName, clusterName)
    actionStop(cmdName, appName, editionName, stopMode, clusterName, nodeName, serverName)
    
    print msgOk % locals()
  except NoFatalWebSphereException, excepcion :
    print scsErr % locals();
    sys.exit(99);
  except :
    exc_type, exc_obj, exc_tb = sys.exc_info()
    print exeErr % (cmdName, sys.exc_type, sys.exc_value, exc_tb.tb_lineno)
    sys.exit(99);

#---------------------------------------------------------------------
# Nombre: parseOptions
# Descripci�n: Procesa los argumentos pasados por el usuario
#---------------------------------------------------------------------
def parseOptions( cmdName ) :
  shortForm = 'sm:cn:nn:sn:an:en:wt:';
  longForm  = 'stopMode=,clusterName=,nodeName=,serverName=,appName=,editionName=,waitingTime='.split( ',' );
  optErr    = '%s: Se ha verificado un error durante el procesamiento: %s\n';

  try :
    opts, args = getopt.getopt( sys.argv, shortForm, longForm );
  except getopt.GetoptError:
    print optErr % (cmdName, sys.exc_value)
    showUsage( cmdName );
  #-------------------------------------------------------------------
  # Creaci�n del diccionario Opts con las claves definidas en longForm
  #-------------------------------------------------------------------
  Opts = {};
  for name in longForm :
    if name[ -1 ] == '=' :
      name = name[ :-1 ]
    Opts[ name ] = None;
        
  #-------------------------------------------------------------------
  # Seteo de valores por omision
  #-------------------------------------------------------------------
  Opts[ 'stopMode' ] = 'Cluster'
  Opts[ 'waitingTime' ] = 300

  #-------------------------------------------------------------------
  # Procesa la lista de opciones retornadas por getopt()
  #-------------------------------------------------------------------
  for opt, val in opts :
    if opt in   ( '-sm', '--stopMode' )               : Opts[ 'stopMode' ] = val
    elif opt in ( '-cn', '--clusterName' )            : Opts[ 'clusterName' ] = val
    elif opt in ( '-nn', '--nodeName' )               : Opts[ 'nodeName' ] = val
    elif opt in ( '-sn', '--serverName' )             : Opts[ 'serverName' ] = val 	
    elif opt in ( '-an', '--appName' )                : Opts[ 'appName' ] = val
    elif opt in ( '-en', '--editionName' )            : Opts[ 'editionName' ] = val
    elif opt in ( '-wt', '--waitingTime' )            : Opts[ 'waitingTime' ] = int(val)

  #-------------------------------------------------------------------
  # Validar relaciones entre par�metros
  #-------------------------------------------------------------------
  if (not Opts[ 'appName' ]):
    print '%(cmdName)s: Error en los parametros, se debe especificar el nombre de la aplicaci�n mediante la opcion --appName' % locals() 
    showUsage( cmdName )
  if (Opts[ 'stopMode' ] == 'Cluster') :
    if (not Opts[ 'clusterName' ]):
      print '%(cmdName)s: Error en los parametros, se debe especificar el nombre del cluster mediante la opcion --clusterName' % locals()
      showUsage( cmdName )
    if (Opts[ 'nodeName' ] or Opts[ 'serverName' ]) :
      print '%(cmdName)s: Error en los parametros, al especificar el frenado en un cluster no se deben especificar las opciones: --serverName, --nodeName' % locals()
      showUsage( cmdName )
  elif (Opts[ 'stopMode' ] == 'Server' ) :
    if (Opts[ 'clusterName' ]):
      print '%(cmdName)s: Error en los parametros, cuando se realiza el frenado en un servidor no se debe especificar la opcion --clusterName' % locals()
      showUsage( cmdName )
      
  #-------------------------------------------------------------------
  # Retorna el diccionario con los argumentos especificados por el usuario
  #-------------------------------------------------------------------
  return Opts;
  
#---------------------------------------------------------------------
# Nombre: showUsage()
# Descripci�n: Rutina utilizada para imprimir en la consola el modo de 
#              utilizaci�n del script.
#---------------------------------------------------------------------
def showUsage( cmdName = None ) :
  if not cmdName :
    cmdName = 'app-stop.py'

  print '''
-----------------------------------------------------------------------------
Nombre: app-stop.py
Descripcion: Script generico para frenar aplicaciones (EAR)
-----------------------------------------------------------------------------
Utilizacion: app-stop.py [opciones]

Opciones soportadas:

 -an | --appName     <valor> = Nombre de la aplicacion
 -en | --editionName <valor> = Nombre de la edicion asociada a la acplicacion
 -sm | --stopMode    <valor> = Especifica si se requiere frenar la aplicacion 
                               en un cluster especifico (Cluster) o en un 
                               servidor determinado (Server)
 -cn | --clusterName <valor> = Nombre del cluster donde sera deplegado el 
                               aplicativo, no aplica si el parametro 
                               --clusterType se establece en None
 -nn | --nodeName    <valor> = Nombre del nodo de despliegue, solo aplicable
                               cuando el parametro --clusterType se establece
                               en None
 -sn | --serverName  <valor> = Nombre del servidor de despliegue, solo
                               aplicable cuando el parametro --clusterType se
                               establece en None 
 -wt | --waitingTime <valor> = Valor en segundos correspondientes al tiempo
                               maximo de espera, de los procesos asincronicos
                               despues de este tiempo se lanzara un error y
                               se debera verifica el resultado de la
                               ejecucion manualmente
                               
Ejemplos:
  wsadmin.sh -f app-stop.py --appName MyApp01
  wsadmin.sh -f app-stop.py --clusterName cluster01 --appName MyApp01 --editionName v01
  wsadmin.sh -f app-stop.py --stopMode Server --serverName MySrv --nodeName MyNodo --appName MyApp01 



  \n''' % locals();
  sys.exit(99);

#---------------------------------------------------------------------
# Clase: NoFatalWebSphereException(Exception)
# Descripci�n: Clase utilizada para el lanzamiento de exepciones de
#              situaciones atrapadas en el c�digo.
#---------------------------------------------------------------------
class NoFatalWebSphereException(Exception):
  pass

#---------------------------------------------------------------------
# Este es el punto de entrada de la ejecuci�n del script
#---------------------------------------------------------------------
if ( __name__ == '__main__' ) :
  cmdName = 'app-stop.py';
  print '%(cmdName)s: iniciando la ejecucion del script %(version)s' % locals()
  Opts = parseOptions( cmdName );
  processStopActions(Opts, cmdName);
else :
  print 'Error: este script debe ejecutarse, no importarse.\n';
  showUsage();
