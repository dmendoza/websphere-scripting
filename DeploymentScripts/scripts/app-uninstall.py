#---------------------------------------------------------------------
# Nombre: app-uninstall.py.py
# Descripcion: Script generico para desinstalar aplicaciones (EAR) 
#
# History
#   fecha     ver  aut     modificacion 
# ----------  ---  ------  -------------------------------------------
# 06/05/2016  0.1  devm    version inicial utilizando getopt
# 24/06/2016  0.2  devm    Adecuciones sysexit
#
# Autores
# --------------------------------------------------------------------
# devm -> diego_mendoza@microgestion
#---------------------------------------------------------------------
import sys, getopt, os.path;
import AdminUtilities;
version = "Version 0.2 2016.06.24"

#---------------------------------------------------------------------
# Nombre: actionUninstallCheck
# Descripcion: Comprueba que los par�metros suministrados sean v�lidos
#              para realizar la tarea de desinstalaci�n
#---------------------------------------------------------------------
def actionUninstallCheck( appName, editionName ) :
  appNameEdition = appName
  if(editionName) :
    appNameEdition = appName + "-edition" + editionName
  appExist = AdminConfig.getid("/Deployment:"+appNameEdition+"/" )
  if (len(appExist) == 0):
    raise NoFatalWebSphereException("La aplicacion especificada '%(appNameEdition)s' NO existe." % locals())

#---------------------------------------------------------------------
# Nombre: actionUninstall
# Descripcion: Realiza la desinstalaci�n de una aplicaci�n
#---------------------------------------------------------------------
def actionUninstall( appName, editionName ):
  try:
    global AdminApp 
    cell = AdminConfig.list("Cell")
    cellName = AdminConfig.showAttribute(cell, "name")
    #--------------------------------------------------------------------
    # Informaci�n sobre la ejecuci�n de actionUninstall
    #--------------------------------------------------------------------
    print "---------------------------------------------------------------"
    print " actionUninstall "
    print "---------------------------------------------------------------"
    print " Nombre de la aplicacion:     "+appName
    if (editionName):
      print " Nombre de la edicion:        "+editionName    
    print "---------------------------------------------------------------"
    #--------------------------------------------------------------------
    # Ejecuci�n de actionUninstall
    #--------------------------------------------------------------------
    appNameEdition = appName
    if(editionName) :
      appNameEdition = appName + "-edition" + editionName    
    AdminApp.uninstall(appNameEdition)
    AdminConfig.save()

  except:
    typ, val, tb = sys.exc_info()
    if (typ==SystemExit):  raise SystemExit, val
    val = "%s %s" % (sys.exc_type, sys.exc_value)
    raise "NoFatalWebSphereException: ", val
    return -1
    
  return 1  # succeed

#---------------------------------------------------------------------
# Nombre: processUninstallActions
# Descripci�n: Determina y ejecuta las tareas necesarias para reaizar  
#              la desinstalaci�n de una aplicacion
#---------------------------------------------------------------------
def processUninstallActions(Opts, cmdName) :
  msgOk = '\n[OK] %(cmdName)s ha realizado la desintalacion de la aplicacion %(appNameEdition)s.'
  scsErr = '%(cmdName)s: Error durante las tareas de desintalacion: %(excepcion)s.'
  exeErr = '%s: Error durante las tareas de desintalacion: %s %s %s'
  #-------------------------------------------------------------------
  # Asigna los valores a variables a partir de los valores del 
  # diccionario de opciones
  #-------------------------------------------------------------------
  for key in Opts.keys() :
    val = Opts[ key ];
    cmd = '%s=Opts["%s"]' % ( key, key );
    exec( cmd );

  try :
    appNameEdition = appName
    if(editionName) :
      appNameEdition = appName + "-edition" + editionName   
      
    actionUninstallCheck(appName, editionName)
    actionUninstall(appName, editionName)
    
    print msgOk % locals()
  except NoFatalWebSphereException, excepcion :
    print scsErr % locals();
    sys.exit(99)    
  except :
    exc_type, exc_obj, exc_tb = sys.exc_info()
    print exeErr % (cmdName, sys.exc_type, sys.exc_value, exc_tb.tb_lineno)
    sys.exit(99)
    
#---------------------------------------------------------------------
# Nombre: parseOptions
# Descripci�n: Procesa los argumentos pasados por el usuario
#---------------------------------------------------------------------
def parseOptions( cmdName ) :
  shortForm = 'an:en:';
  longForm  = 'appName=,editionName='.split( ',' );
  optErr    = '%s: Se ha verificado un error durante el procesamiento: %s\n';

  try :
    opts, args = getopt.getopt( sys.argv, shortForm, longForm );
  except getopt.GetoptError:
    print optErr % (cmdName, sys.exc_value)
    showUsage( cmdName );

  #-------------------------------------------------------------------
  # Creaci�n del diccionario Opts con las claves definidas en longForm
  #-------------------------------------------------------------------
  Opts = {};
  for name in longForm :
    if name[ -1 ] == '=' :
      name = name[ :-1 ]
    Opts[ name ] = None;

  #-------------------------------------------------------------------
  # Procesa la lista de opciones retornadas por getopt()
  #-------------------------------------------------------------------
  for opt, val in opts :
    if opt in   ( '-an', '--appName' )              : Opts[ 'appName' ] = val
    elif opt in ( '-en', '--editionName' )          : Opts[ 'editionName' ] = val

  #-------------------------------------------------------------------
  # Validar relaciones entre par�metros
  #-------------------------------------------------------------------
  if (not Opts[ 'appName' ]):
    print '%(cmdName)s: Error en los parametros, se debe especificar el nombre de la aplicaci�n mediante la opcion --appName' % locals() 
    showUsage( cmdName )

  #-------------------------------------------------------------------
  # Retorna el diccionario con los argumentos especificados por el usuario
  #-------------------------------------------------------------------
  return Opts;
  
#---------------------------------------------------------------------
# Nombre: showUsage()
# Descripci�n: Rutina utilizada para imprimir en la consola el modo de 
#              utilizaci�n del script.
#---------------------------------------------------------------------
def showUsage( cmdName = None ) :
  if not cmdName :
    cmdName = 'app-uninstall.py'

  print '''
-----------------------------------------------------------------------------
Nombre: app-uninstall.py
Descripcion: Script generico para desinstalar aplicaciones (EAR)
-----------------------------------------------------------------------------
Utilizacion: app-uninstall.py [opciones]

Opciones soportadas:
 -an | --appName     <valor> = Nombre de la aplicacion
 -en | --editionName <valor> = Nombre de la edicion asociada a la acplicacion

Ejemplos:
  wsadmin.sh -f app-uninstall.py.py --appName MyApp01
  wsadmin.sh -f app-uninstall.py.py --appName MyApp01 --editionName v01
    
  \n''' % locals();
  sys.exit(99);

#---------------------------------------------------------------------
# Clase: NoFatalWebSphereException(Exception)
# Descripci�n: Clase utilizada para el lanzamiento de exepciones de
#              situaciones atrapadas en el c�digo.
#---------------------------------------------------------------------
class NoFatalWebSphereException(Exception):
  pass

#---------------------------------------------------------------------
# Este es el punto de entrada de la ejecuci�n del script
#---------------------------------------------------------------------
if ( __name__ == '__main__' ) :
  cmdName = 'app-uninstall.py';
  print '%(cmdName)s: iniciando la ejecucion del script %(version)s' % locals()
  Opts = parseOptions( cmdName );
  processUninstallActions(Opts, cmdName);
else :
  print 'Error: este script debe ejecutarse, no importarse.\n';
  showUsage();