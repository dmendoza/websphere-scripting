#---------------------------------------------------------------------
# Nombre: app-install.py
# Descripcion: Script generico de despliegue de aplicaciones (EAR) 
#
# History
#   fecha     ver  aut     modificacion 
# ----------  ---  ------  -------------------------------------------
# 04/05/2016  0.1  devm    version inicial utilizando getopt
# 07/05/2016  0.2  devm    multiples servidores web
# 17/05/2016  0.3  devm    se considera websphere.cluster.partial.*
# 19/05/2016  0.4  devm    archivos de roles y sess manager
# 20/05/2016  0.5  devm    archivo de librerias compartidas
# 26/05/2016  0.6  devm    sess manager se pasa a app-edit.py
# 31/05/2016  0.7  devm    eliminacion de impresiones para debug
# 24/06/2016  0.8  devm    Adecuciones sysexit
# 16/07/2016  0.9  devm    Se incorpora el parametro defMapDS
# 21/07/2016  0.10 devm    correcci�n cuando defMapDS=No
#
# Autores
# --------------------------------------------------------------------
# devm -> diego_mendoza@microgestion.com
#---------------------------------------------------------------------
import sys, time, getopt, os.path;
import AdminUtilities;
version = "Version 0.10 2016.07.21"

#---------------------------------------------------------------------
# Nombre: actionDeployCheck
# Descripcion: Comprueba que los par�metros suministrados sean v�lidos
#              para realizar la tarea de despliegue
#---------------------------------------------------------------------
def actionDeployCheck( cmdName, appName, editionName, updateExist, earFile, clusterName, rolesFile, libsFile) :
  msgErrEarFileNotFound   = "No se pudo encontrar el archivo EAR en la ruta especificada '%(earFile)s'.";
  msgErrRolesFileNotFound = "No se pudo encontrar el archivo de especificacion de Roles en la ruta especificada '%(rolesFile)s'.";
  msgErrSessFileNotFound  = "No se pudo encontrar el archivo de especificacion de parametros del Gestor de Sesiones en la ruta especificada '%(sessMgrFile)s'.";
  msgErrLibsFileNotFound  = "No se pudo encontrar el archivo de especificacion de Librerias Compartidas en la ruta especificada '%(sessMgrFile)s'.";
  msgErrClusterWrongState = "El cluster especificado '%(clusterName)s' no esta en un estado adecuado."
  msgErrClusterNotRunning = "El cluster especificado '%(clusterName)s' no esta en ejecucion."
  msgErrClusterNotFound   = "El cluster especificado '%(clusterName)s' no existe."
  msgErrAppExist          = "La aplicacion especificada '%(appNameEdition)s' ya existe."
  msgAppOverwrite         = "%(cmdName)s: La aplicacion especificada '%(appNameEdition)s' ya existe, la misma ser� actualizada."
  msgClusterState         = "%(cmdName)s: El cluster especificado '%(clusterName)s' se encuentra en el estado '%(clusterState)s'."
  
  #--------------------------------------------------------------------
  # Verificaciones para despliegue en cluster
  #--------------------------------------------------------------------
  if (clusterName):
    clusterExist = AdminConfig.getid("/ServerCluster:"+clusterName+"/")
    if (len(clusterExist) == 0):
      raise NoFatalWebSphereException(msgErrClusterNotFound % locals())
  
    clusterMbean = AdminControl.completeObjectName("type=Cluster,name="+clusterName+",*")
    if (len(clusterMbean) == 0):
      raise NoFatalWebSphereException( msgErrClusterNotRunning % locals())
    clusterState = AdminControl.getAttribute(clusterMbean, "state")
    print msgClusterState % locals()
    if (clusterState != "websphere.cluster.running"):
      if (clusterState != "websphere.cluster.partial.start"):
        if (clusterState != "websphere.cluster.partial.stop"):
          raise NoFatalWebSphereException(msgErrClusterWrongState % locals())
  #--------------------------------------------------------------------
  # Verificaciones de existencia de archivos
  #--------------------------------------------------------------------
  fileExist = os.path.isfile(earFile);
  if (not fileExist):
    raise NoFatalWebSphereException( msgErrEarFileNotFound % locals())
  
  if (rolesFile):
    fileExist = os.path.isfile(rolesFile);
    if (not fileExist):
      raise NoFatalWebSphereException( msgErrRolesFileNotFound % locals())
  
  if (libsFile):
    fileExist = os.path.isfile(libsFile);
    if (not fileExist):
      raise NoFatalWebSphereException( msgErrLibsFileNotFound % locals())
  #--------------------------------------------------------------------
  # Verificaciones de existencia de la aplicaci�n
  #--------------------------------------------------------------------
  appNameEdition = appName
  if(editionName) :
    appNameEdition = appName + "-edition" + editionName
  
  appExist = AdminConfig.getid("/Deployment:"+appNameEdition+"/" )
  if (len(appExist) > 0):
    if(updateExist=='Yes'): print msgAppOverwrite % locals()
    else: raise NoFatalWebSphereException( msgErrAppExist % locals())
  
#---------------------------------------------------------------------
# Nombre: actionDeploy
# Descripcion: Realiza el despliegue de una aplicaci�n
#---------------------------------------------------------------------
def actionDeploy( appName, editionName, updateExist, earFile, clusterName, nodeName, serverName, webServers, virtualHostName, rolesDefinition, libsDefinition, defMapDS ):
  try:
    global AdminApp 
    mode = 'INSTALL'
    cell = AdminConfig.list("Cell")
    cellName = AdminConfig.showAttribute(cell, "name")
    #--------------------------------------------------------------------
    # Identificaci�n de modalidad de operacion: UPDATE o INSTALL
    #--------------------------------------------------------------------   
    appNameEdition = appName
    if(editionName) :
      appNameEdition = appName + "-edition" + editionName     
    if (updateExist=='Yes'):
      appExist = AdminConfig.getid("/Deployment:"+appNameEdition+"/" )
      if (len(appExist) > 0):
        mode = 'UPDATE'
      
    #--------------------------------------------------------------------
    # Informaci�n sobre la ejecuci�n de actionDeploy
    #--------------------------------------------------------------------
    print "---------------------------------------------------------------"
    print " actionDeploy "
    print "---------------------------------------------------------------"
    print " Nombre de la aplicacion:     "+appName
    if (editionName):
      print " Nombre de la edicion:        "+editionName
    print " Modalidad de la operacion:     "+mode
    print " Archivo EAR:                 "+earFile
    print " Nombre de la celda:          "+cellName
    if (clusterName):
      print " Nombre del cluster:          "+clusterName
    else:
      print " Nombre del nodo:             "+nodeName
      print " Nombre del server:           "+serverName
    print " Virtual host:                "+virtualHostName
    for webServer in webServers:
      print " Web server:                  "+webServer
    #--------------------------------------------------------------------
    # Verificaci�n de conformaci�n de m�dulos presentes en el EAR
    #--------------------------------------------------------------------
    print "---------------------------------------------------------------"
    print " Modulos en EAR "
    print "---------------------------------------------------------------"    
    modules = AppModules(earFile = earFile)
    for mod in modules.modules:
      print " Modulo detectado:            ",mod.uri
    print " Possee modulos Web:          ",modules.showHasWebModules()
    print " Possee modulos Ejb:          ",modules.showHasEjbModules()
    print "---------------------------------------------------------------"
    print " Mapeo por omision de Fuentes de Datos (javax.sql.DataSource)"
    print "---------------------------------------------------------------"
    print " Mapeo por omision activado:  ", defMapDS
    if (defMapDS=='Yes'):
      refs = AppDataSourceRefs(earFile = earFile)
      print " Numero de referencia:        ", len(refs.refs)
      for ref in refs.refs:
        print "     Referencia en el modulo " + ref.module + " --> " + ref.targetResJNDI
    print "---------------------------------------------------------------"

    if (libsDefinition):
    #--------------------------------------------------------------------
    # Dump shared libs definition
    #--------------------------------------------------------------------
      print " Configuracion de librerias compartidas "
      print "---------------------------------------------------------------"
      print " A nivel de aplicacion:       "+libsDefinition.showHasApplicationLevelLibs()
      for lib in libsDefinition.applicationLevelLibs:
        print "              Libreria:       * "+lib
      print " A nivel de modulo Web:       "+libsDefinition.showHasWebModuleLevelLibs()
      for lib in libsDefinition.webModuleLevelLibs:
        print "              Libreria:       * "+lib      
      print "---------------------------------------------------------------" 
      
    if (rolesDefinition):
    #--------------------------------------------------------------------
    # Dump roles definition
    #--------------------------------------------------------------------
      print " Configuracion de roles "
      if (rolesDefinition) :
        for role in rolesDefinition.roles:
          print "---------------------------------------------------------------" 
          print " Nombre del rol definido:      "+role.name
          print "   Subject Authenticated:      "+role.showIsSubjectAuthenticated()
          print "        Subject Everyone:      "+role.showIsSubjectEveryone()
          for user in role.users:
            print "               - Usuario:      * "+user.cn
          for group in role.groups:
            print "                 - Grupo:      * "+group.cn
        print "---------------------------------------------------------------"
    #--------------------------------------------------------------------
    # Ejecuci�n de actionDeployToCluster
    #--------------------------------------------------------------------
    if (clusterName):
      mapping_target = "WebSphere:cell="+cellName+",cluster="+clusterName
    else:
      mapping_target = "WebSphere:cell="+cellName+",node="+nodeName+",server="+serverName
    mapping_target_webservers = mapping_target
    for webServer in webServers:
      mapping_target_webservers += "+WebSphere:cell="+cellName+","+webServer

    mapping_ejb = [".*", ".*.jar,.*", mapping_target]
    mapping_war = [".*", ".*.war,.*", mapping_target_webservers]
    
    mapping = []
    if (modules.hasEjbModules):
      mapping.append(mapping_ejb)
    if (modules.hasWebModules):
      mapping.append(mapping_war)
    
    ds_mapping = '' #[]
    if (defMapDS=='Yes'):
      for ref in refs.refs:
        ds_mapping = ds_mapping + '[ '+ ref.module + ' "" ' + ref.uri + ' ' + ref.resRef + ' javax.sql.DataSource ' + ref.targetResJNDI +  ' "" "" "" ]'
      opts = ["-MapResRefToEJB", '['+ ds_mapping +']']
      opts += ["-MapWebModToVH", [['.*', '.*', virtualHostName]]] 
    else :
      opts = ["-MapWebModToVH", [['.*', '.*', virtualHostName]]] 
    
    opts += ["-MapModulesToServers", mapping]   
    if (editionName):    
      opts += ['-edition', editionName]
    opts += ['-appname', appName]
    
    if (rolesDefinition):
      mapping_users = rolesDefinition.getMapRolesToUserString()
      opts += ['-MapRolesToUsers', '['+ mapping_users +']'] 

    if (libsDefinition):
      mapping_libs = libsDefinition.getMapSharedLibForMod(appName = appNameEdition, appModules = modules)
      opts += ['-MapSharedLibForMod', '['+ mapping_libs +']'] 

    if (mode=='INSTALL'): 
      AdminApp.install( earFile, opts)
    elif (mode=='UPDATE'):
      opts += ['-operation', 'update']  
      opts += ['-contents', earFile] 
      AdminApp.update( appNameEdition, 'app', opts)
    AdminConfig.save()

  except:
    typ, val, tb = sys.exc_info()
    if (typ==SystemExit):  raise SystemExit, val
    val = "%s %s" % (sys.exc_type, sys.exc_value)
    raise "NoFatalWebSphereException: ", val
    return -1
    
  return 1  # succeed

#---------------------------------------------------------------------
# Nombre: actionDeployToClusterWaitingForSuccess
# Descripcion: Espera que la aplicaci�n se encuentre instalada y lista
#              para ser iniciada
#---------------------------------------------------------------------
def actionDeployToClusterWaitingForSuccess(appName, editionName, waitingTime) :
  msgResult = "La aplicacion %(appNameEdition)s se propago correctamente? --> %(result)s."
  sleepTime = 10 # 10 segundos
  waitingCycle = 0
  maxWaitingCycles = int(300 / 10)
  
  appNameEdition = appName
  if(editionName) :
    appNameEdition = appName + "-edition" + editionName  

  result = AdminApp.isAppReady(appNameEdition)
  
  while ( result == "false" and waitingCycle < maxWaitingCycles):
    waitingCycle = waitingCycle + 1
    print "Esperando %i segundos." % sleepTime
    time.sleep(sleepTime)
    result = AdminApp.isAppReady(appNameEdition) 
    print msgResult % locals() 
  if waitingCycle == maxWaitingCycles :
    val = "Error al distribuir aplicacion. Tiempo de espera agotado. Realizar refresh."
    print val
    raise "WaitingForSuccessException: ", val

#---------------------------------------------------------------------
# Nombre: processDeploymentActions
# Descripci�n: Determina y ejecuta las tareas necesarias para reaizar  
#              el despliegue de una aplicacion
#---------------------------------------------------------------------
def processDeploymentActions(Opts, cmdName) :
  msgOkCluster = '\n[OK] %(cmdName)s ha realizado el despliegue de la aplicacion en el cluster %(clusterName)s.'	
  msgOkServer = '\n[OK] %(cmdName)s ha realizado el despliegue de la aplicacion en el server %(serverName)s.'
  wfcErr = '%(cmdName)s: Error durante las tareas de despliegue: %(excepcion)s'
  scsErr = '%(cmdName)s: Error durante las tareas de despliegue: %(excepcion)s'
  exeErr = '%s: Error durante las tareas de despliegue: %s %s %s'
  #-------------------------------------------------------------------
  # Asigna los valores a variables a partir de los valores del 
  # diccionario de opciones
  #-------------------------------------------------------------------
  for key in Opts.keys() :
    val = Opts[ key ];
    cmd = '%s=Opts["%s"]' % ( key, key );
    exec( cmd );
  
  #if (Opts[ 'clusterName' ]):
  try :
    actionDeployCheck(cmdName, appName, editionName, updateExist, earFile, clusterName, rolesFile, libsFile)
    rolesDefinition = None
    if (rolesFile):
      rolesDefinition = RolesDefinition(rolesFile = rolesFile)
    libsDefinition = None
    if (libsFile):
      libsDefinition = SharedLibsDefinition(libsFile = libsFile)      
    actionDeploy(appName, editionName, updateExist, earFile, clusterName, nodeName, serverName, webServer, virtualHost, rolesDefinition, libsDefinition, defMapDS)
    actionDeployToClusterWaitingForSuccess(appName, editionName, waitingTime)
    if (clusterName):
      print msgOkCluster % locals()
    else:
       print msgOkServer % locals()
  except WaitingForSuccessException, excepcion :
    print wfcErr % locals();
    sys.exit(99)
  except NoFatalWebSphereException, excepcion :
    print scsErr % locals();
    sys.exit(99)
  except :
    exc_type, exc_obj, exc_tb = sys.exc_info()
    print exeErr % (cmdName, sys.exc_type, sys.exc_value, exc_tb.tb_lineno)
    sys.exit(99)
  
#---------------------------------------------------------------------
# Nombre: parseOptions
# Descripci�n: Procesa los argumentos pasados por el usuario
#---------------------------------------------------------------------
def parseOptions( cmdName ) :
  shortForm = 'ct:cn:nn:sn:an:en:ue:ef:vh:ww:rf::lf:ds:wt:';
  longForm  = 'clusterType=,clusterName=,nodeName=,serverName=,appName=,editionName=,updateExist=,earFile=,virtualHost=,webServer=,rolesFile=,libsFile=,defMapDS=,waitingTime='.split( ',' );
  badOpt    = '%(cmdName)s: Parametro desconocido %(plural)s: %(argStr)s\n';
  optErr    = '%s: Se ha verificado un error durante el procesamiento de las opciones: %s\n';

  try :
    opts, args = getopt.getopt( sys.argv, shortForm, longForm );
  except getopt.GetoptError:
    print optErr % (cmdName, sys.exc_value)
    showUsage( cmdName );

  #-------------------------------------------------------------------
  # Creaci�n del diccionario Opts con las claves definidas en longForm
  #-------------------------------------------------------------------
  Opts = {};
  for name in longForm :
    if name[ -1 ] == '=' :
      name = name[ :-1 ]
    Opts[ name ] = None;

  #-------------------------------------------------------------------
  # Seteo de valores por omision
  #-------------------------------------------------------------------
  Opts[ 'clusterType' ] = 'Dynamic'
  Opts[ 'virtualHost' ] = 'default_host'
  Opts[ 'virtualHost' ] = 'default_host'
  Opts[ 'updateExist' ] = 'No'
  Opts[ 'defMapDS' ]    = 'No'
  Opts[ 'webServer' ]   = []
  Opts[ 'waitingTime' ] = 300

  #-------------------------------------------------------------------
  # Procesa la lista de opciones retornadas por getopt()
  #-------------------------------------------------------------------
  for opt, val in opts :
    if opt in   ( '-ct', '--clusterType' )            : Opts[ 'clusterType' ] = val
    elif opt in ( '-cn', '--clusterName' )            : Opts[ 'clusterName' ] = val
    elif opt in ( '-nn', '--nodeName' )               : Opts[ 'nodeName' ] = val
    elif opt in ( '-sn', '--serverName' )             : Opts[ 'serverName' ] = val
    elif opt in ( '-an', '--appName' )                : Opts[ 'appName' ] = val
    elif opt in ( '-en', '--editionName' )            : Opts[ 'editionName' ] = val
    elif opt in ( '-ue', '--updateExist' )            : Opts[ 'updateExist' ] = val
    elif opt in ( '-ef', '--earFile' )                : Opts[ 'earFile' ] = val
    elif opt in ( '-vh', '--virtualHost' )            : Opts[ 'virtualHost' ] = val
    elif opt in ( '-ww', '--webServer' )              : Opts[ 'webServer' ].append(val)
    elif opt in ( '-rf', '--rolesFile' )              : Opts[ 'rolesFile' ] = val
    elif opt in ( '-lf', '--libsFile' )               : Opts[ 'libsFile' ] = val
    elif opt in ( '-ds', '--defMapDS' )               : Opts[ 'defMapDS' ] = val
    elif opt in ( '-wt', '--waitingTime' )            : Opts[ 'waitingTime' ] = int(val)

  #-------------------------------------------------------------------
  # Validar relaciones entre par�metros
  #-------------------------------------------------------------------
  if (not Opts[ 'appName' ]):
    print '%(cmdName)s: Error en los parametros, se debe especificar el nombre de la aplicaci�n mediante la opcion --appName' % locals() 
    showUsage( cmdName )
  if (not Opts[ 'earFile' ]):
    print '%(cmdName)s: Error en los parametros, se debe especificar la ubicaci�n del archivo .ear mediante la opcion --earFile' % locals() 
    showUsage( cmdName )
  if (Opts[ 'clusterType' ] in ( 'Dynamic', 'Static' )):
    if (not Opts[ 'clusterName' ]):
      print '%(cmdName)s: Error en los parametros, se debe especificar el nombre del cluster mediante la opcion --clusterName' % locals() 
      showUsage( cmdName )
    if (Opts[ 'nodeName' ]) or (Opts[ 'serverName' ]):
      print '%(cmdName)s: Error en los parametros, al especificar el despliegue en un cluster no se deben especificar las opciones: --serverName, --nodeName' % locals() 
      showUsage( cmdName )
  else :
    if (Opts[ 'clusterName' ]):
      print '%(cmdName)s: Error en los parametros, cuando se realiza el despliegue fuera de un cluster no se debe especificar la opcion --clusterName' % locals() 
      showUsage( cmdName )
#  if (Opts[ 'clusterType' ] != 'Dynamic'):  	
#    if (Opts[ 'webServer' ]) or (Opts[ 'webServer' ]):
#      print '%(cmdName)s: Error en los par�metros, la opcion --editionName solo se debe especificar en el despliegue en un cluster din�mico';
#      showUsage( cmdName );

  #-------------------------------------------------------------------
  # Retorna el diccionario con los argumentos especificados por el usuario
  #-------------------------------------------------------------------
  return Opts;

#---------------------------------------------------------------------
# Nombre: parseAppTaskInfo
# Descripci�n: Procesa el resultado de le ejcuci�n de la tarea 
#              AdminApp.taskInfo y retorna un diccionario con los
#              valores
#---------------------------------------------------------------------
def parseAppTaskInfo(lines, headerLine = 'The current contents of the task after running default bindings are:'):
  records = []
  inHeader = 1 # Estoy en el encabezado
  for line in lines:
    # Esquivo el encabezado
    if (inHeader == 1):
      if (line == headerLine):
        inHeader = 0
        record = {}
      continue

    # Sali del encabezado
    if (line and line.strip()):
      line = line.strip()
      # Omito si no es del tipo Parametro:Valor
      if (line.find(':') < 1):
        continue
      # Si es nuevo, agrego registro a la colecci�n
      if (not record):
        records.append(record)
      (key, value) = [ item.strip() for item in line.split(':',1) ]
      record[key] = value
    elif (record):
      # Inicializo nuevo registro cuando aparece una linea en blanco
      record = {}
  return records

#---------------------------------------------------------------------
# Nombre: showUsage()
# Descripci�n: Rutina utilizada para imprimir en la consola el modo de 
#              utilizaci�n del script.
#---------------------------------------------------------------------
def showUsage( cmdName = None ) :
  if not cmdName :
    cmdName = 'app-install.py'

  print '''
---------------------------------------------------------------------
Nombre: app-install.py
Descripcion: Script generico para el despliegue de aplicaciones.
---------------------------------------------------------------------
Utilizacion: app-install.py [opciones]

Opciones soportadas:

 -an | --appName     <valor> = Nombre de la aplicacion
 -en | --editionName <valor> = Nombre de la edicion asociada a la acplicacion
 -ef | --earFile     <valor> = Ubicacion completa al archivo .ear
 -ue | --updateExist <valor> = Si se encuentra una aplicaci�n con el nombre 
                               especificado (--appName y --editionName) ser� 
                               actualizada siempre que el parametro --updateExist 
                               se encuentre en Yes, de lo contrario el script 
                               finalizara con un mensaje de error
 -ct | --clusterType <valor> = Tipo de despliegue (Dynamic, Static o None),
                               utilizar None para realizar el despliegue 
                               en un servidor
 -cn | --clusterName <valor> = Nombre del cluster donde sera deplegado el 
                               aplicativo, no aplica si el parametro 
                               --clusterType se establece en None
 -nn | --nodeName    <valor> = Nombre del nodo de despliegue, solo aplicable
                               cuando el parametro --clusterType se establece
                               en None
 -sn | --serverName  <valor> = Nombre del servidor de despliegue, solo
                               aplicable cuando el parametro --clusterType se
                               establece en None
 -ds | --defMapDS    <valor> = Permite correlacionar las fuentes de datos
                               (javax.sql.DataSource) de manera automatica 
                               utilizando el nombre destino JNDI utilizado
                               dentro de la aplicaci�n. El valor por omision es 
                               No, si se especifica Yes el nombre JNDI de los
                               recursos definidos dentro del servidor deben
                               ser identicos a los de la aplicacion.
 -vh | --virtualHost <valor> = Nombre del Virtual Host que sera asignado a
                               todos los modulos Web presentes en la
                               aplicacion
 -ww | --webServer   <valor> = Nombre del nodo y del servidor Web que sera 
                               asignado a todos los Modulos Web presentes en
                               la aplicacion. Ver Notas.
 -rf | --rolesFile   <valor> = Ruta completa al archivo donde se encuentran
                               la correlacion de usuario y grupos con los
                               roles definidos dentro de la aplicacion
 -lf | --libsFile    <valor> = Ruta completa al archivo donde se encuentra la
                               correlacion de librerias compartidas con la 
                               aplicacion y cada modulo particular
 -wt | --waitingTime <valor> = Valor en segundos correspondientes al tiempo
                               maximo de espera, de los procesos asincronicos
                               despues de este tiempo se lanzara un error y
                               se debera verifica el resultado de la
                               ejecucion manualmente
                               
Notas:

* Si no se especifica --clusterType sera considerado Dynamic como valor por
  defecto
* Si no se especifica --virtualHost sera considerado default_host como valor
  por defecto
* El formato para la opcion --webServer es Node=NombreNodo,   
  server=NombreServer
* Se pueden utilizar la opcion --webServer multiples veces para especificar
  la asignacion de mas de un servidor web al aplicativo
* Si no se especifica --waitingTime se utilizara un tiempo de espera de 300
  segundos

Ejemplos:

  wsadmin.sh -f app-install.py.py --appName MyApp01 --earFile /apps/app1.ear --clusterName C1
  wsadmin.sh -f app-install.py.py --appName MyApp02 --earFile /apps/app2.ear --clusterName C2 --virtualHost otro_host_virtual
  wsadmin.sh -f app-install.py.py --appName MyApp02 --earFile /apps/app3.ear --clusterName C3 --webServer node=webihsdl01-rc,server=webihsdl01

  \n''' % locals();
  sys.exit(99);

#---------------------------------------------------------------------
# Clase: NoFatalWebSphereException(Exception)
# Descripci�n: Clase utilizada para el lanzamiento de exepciones de
#              situaciones atrapadas en el c�digo.
#---------------------------------------------------------------------
class NoFatalWebSphereException(Exception):
  pass

#---------------------------------------------------------------------
# Clase: WaitingForSuccessException(Exception)
# Descripci�n: Clase utilizada para el lanzamiento de exepciones
#              asociadas al agotamiento del tiempo de espera.
#---------------------------------------------------------------------
class WaitingForSuccessException(Exception):
  pass

#---------------------------------------------------------------------
# Clase: SharedLibsDefinition
# Descripci�n: Encapsula la obtenci�n de los nombres de la librerias
#              compartidas que se debe configurar a nivel de aplicacion
#              y a nivel de modulo web.
#---------------------------------------------------------------------
class SharedLibsDefinition:
  def __init__(self, libsFile):
    self.hasApplicationLevelLibs= 0
    self.hasWebModuleLevelLibs = 0  	
    self.applicationLevelLibs = []
    self.webModuleLevelLibs = []
    definitionFile = open(libsFile, 'r')
    data = definitionFile.readlines( )
    definitionFile.close()
    for line in data:
      if (line.find('[Application]')==0):
        currentLevel = 'app'
        self.hasApplicationLevelLibs=1
      elif (line.find('[WebModule]')==0): 
        currentLevel = 'web'   
        self.hasWebModuleLevelLibs=1
      elif (line.find('sharedlib=')==0): 
        libName = line[10:len(line)-1]
        if (currentLevel == 'app'): self.applicationLevelLibs.append(libName)
        else: self.webModuleLevelLibs.append(libName)
    #print self.applicationLevelLibs
    #print self.webModuleLevelLibs
  def getMapSharedLibForMod(self, appName, appModules):
    res = ''
    separator = '+'
    if(self.hasApplicationLevelLibs==1):
      libs = separator.join(self.applicationLevelLibs)
      res = '[' + appName + ' META-INF/application.xml ' + libs + ']'
    if(self.hasWebModuleLevelLibs==1):
      libs = separator.join(self.webModuleLevelLibs)
      for mod in appModules.modules:   
        res += '[' + mod.module + ' ' + mod.uri + ' ' + libs + ']'
    return res
          
  def showHasApplicationLevelLibs(self):
    if (self.hasApplicationLevelLibs==1): return 'Yes'
    else: return 'No'
  def showHasWebModuleLevelLibs(self):
    if (self.hasWebModuleLevelLibs==1): return 'Yes'
    else: return 'No'
#---------------------------------------------------------------------
# Clase: RolesDefinition
# Descripci�n: Encapsula la obtenci�n de de los roles requeridos para
#              configurar posteriormente la correlaci�n de usuarios y
#              grupos con los roles definidos en una aplicaci�n.
#---------------------------------------------------------------------
class RolesDefinition:
  def __init__(self, rolesFile):
    self.roles = []
    definitionFile = open(rolesFile, 'r')
    data = definitionFile.readlines( )
    definitionFile.close()
    for line in data:
      if (line.find('[')==0):
        currentRole = line
        role = RoleDefinition(name = currentRole)
        self.roles.append(role)
      elif (line.find('user=')==0):
        role.addUser(userInfo = line)
      elif (line.find('group=')==0):
        role.addGroup(groupInfo = line)
      elif (line.find('special=AllAuthenticated')==0):
        role.isSubjectAuthenticated = 1
      elif (line.find('special=Everyone')==0):
        role.isSubjectEveryone = 1

  def getMapRolesToUserString(self):
      res = ''
      for role in self.roles:
        res += role.getMapRolesToUserString()
      return res
#---------------------------------------------------------------------
# Clase: RoleDefinition
# Descripci�n: Encapsula los atributos de un m�dulo de aplicaci�n
#---------------------------------------------------------------------
class RoleDefinition:
  def __init__(self, name):
    self.isSubjectAuthenticated = 0;
    self.isSubjectEveryone = 0;
    self.users = []
    self.groups = []
    self.name = name[1:len(name)-2]
  def getMapRolesToUserString(self):
    res  = self.name 
    res += ' AppDeploymentOption.' + self.showIsSubjectEveryone()
    res += ' AppDeploymentOption.' + self.showIsSubjectAuthenticated()
    res += ' "' + self.getCnForUsers() + '"'
    res += ' "' + self.getCnForGroups() + '"'
    res += ' AppDeploymentOption.No'
    res += ' "' + self.getCannonicalForUsers() + '"'
    res += ' "' + self.getCannonicalForGroups() + '"'
    return '['+res+']'
    #return [res]
     
  def addUser(self, userInfo):
    user = Identity(type ='user', info = userInfo)
    self.users.append(user)
    #print 'Se agrega usuario '+user.cn+' en el rol: ' + self.name
    
  def addGroup(self, groupInfo):
    group = Identity(type ='group', info = groupInfo)
    self.groups.append(group)
    #print 'Se agrega grupo '+group.cn+' en el rol: ' + self.name
  
  def getCnForGroups(self):
    return self.getCn(identities = self.groups)
  def getCnForUsers(self):
    return self.getCn(identities = self.users)
  def getCannonicalForGroups(self):
    return self.getCannonical(identities = self.groups)
  def getCannonicalForUsers(self):
    return self.getCannonical(identities = self.users)
    
  def getCn(self, identities):
    res = ''
    for identity in identities:
      if(res==''): res = identity.cn 
      else: res += '|' + identity.cn
    return res
    
  def getCannonical(self, identities):
    res = ''
    for identity in identities:
      if(res==''): res = identity.cannonical 
      else: res += '|' + identity.cannonical
    return res

  def showIsSubjectAuthenticated(self):
    if (self.isSubjectAuthenticated==1): return 'Yes'
    else: return 'No'
  def showIsSubjectEveryone(self):
    if (self.isSubjectEveryone==1): return 'Yes'
    else: return 'No'
#---------------------------------------------------------------------
# Clase: Identity
# Descripci�n: Encapsula los atributos de un usuario o grupo
#---------------------------------------------------------------------
class Identity:
  def __init__(self, type, info):
    self.type = type
    info2 = info[len(type)+1:len(info)-1].split('###')
    self.cn = info2[0]
    if (type=='user'): self.cannonical = 'user:' + info2[1]
    else: self.cannonical = 'group:' + info2[1]
    

#---------------------------------------------------------------------
# Clase: AppDataSourceRefs
# Descripci�n: Encapsula la obtenci�n de las referencias a fuentes de
#              datos (javax.sql.DataSource) dentro del EAR
#---------------------------------------------------------------------
class AppDataSourceRefs:
  def __init__(self, earFile):
    self.refs = []
    for record in parseAppTaskInfo(AdminApp.taskInfo(earFile, 'MapResRefToEJB').splitlines()):
      if (record['Resource type']=='javax.sql.DataSource'):
        ref = AppDataSourceRef(
          module = record['Module'], 
          uri = record['URI'],
          resRef = record['Resource Reference'],
          resType = record['Resource type'],
          targetResJNDI = record['Target Resource JNDI Name']
        )
        self.refs.append(ref)

#---------------------------------------------------------------------
# Clase: AppDataSourceRef
# Descripci�n: Encapsula los atributos de una referencia a un DS
#---------------------------------------------------------------------
class AppDataSourceRef:
  def __init__(self, module, uri, resRef, resType, targetResJNDI):
    self.module = module
    self.uri = uri
    self.resRef = resRef
    self.resType = resType
    self.targetResJNDI = targetResJNDI

#---------------------------------------------------------------------
# Clase: AppModules
# Descripci�n: Encapsula la obtenci�n de los m�dulos contenidos en un 
#              archivo .EAR
#---------------------------------------------------------------------
class AppModules:
  def __init__(self, earFile):
    self.hasWebModules = 0
    self.hasEjbModules = 0
    self.modules = []
    for record in parseAppTaskInfo(AdminApp.taskInfo(earFile, 'MapModulesToServers').splitlines()):
      mod = AppModule(module = record['Module'], uri = record['URI'])
      if (mod.uri.find('ejb-jar.xml') > 0):
        self.hasEjbModules = 1
      if (mod.uri.find('web.xml') > 0):
        self.hasWebModules = 1   
      self.modules.append(mod)

  def showHasWebModules(self):
    if (self.hasWebModules==1): return 'Yes'
    else: return 'No'
  def showHasEjbModules(self):
    if (self.hasEjbModules==1): return 'Yes'
    else: return 'No'
#---------------------------------------------------------------------
# Clase: AppModule
# Descripci�n: Encapsula los atributos de un m�dulo de aplicaci�n
#---------------------------------------------------------------------
class AppModule:
  def __init__(self, module, uri):
    self.module = module
    self.uri = uri
  
#---------------------------------------------------------------------
# Este es el punto de entrada de la ejecuci�n del script
#---------------------------------------------------------------------
if ( __name__ == '__main__' ) :
  cmdName = 'app-install.py';
  print '%(cmdName)s: iniciando la ejecucion del script %(version)s' % locals()
  Opts = parseOptions( cmdName );
  processDeploymentActions(Opts, cmdName);
else :
  print 'Error: este script debe ejecutarse, no importarse.\n';
  showUsage();





