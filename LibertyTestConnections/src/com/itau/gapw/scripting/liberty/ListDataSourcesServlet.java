package com.itau.gapw.scripting.liberty;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


/**
 * diego_mendoza@microgestion.com
 * Servlet para listar las fuentes de datos
 */
@WebServlet(urlPatterns="/*", loadOnStartup=1)
public class ListDataSourcesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	   
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ListDataSourcesServlet() {
		super();
		System.out.println("ListDataSourcesServlet initialization...");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String s_param_jndi = request.getParameter("jndi");
		//System.out.println(s_param_jndi);
		if(s_param_jndi==null){
			listDataSources(out);
		}else{
			testDataSources(out, s_param_jndi);
		}
		
	}

	/**
	 * @param out
	 */
	private void testDataSources(PrintWriter out, String s_param_jndi){
		try {
			out.println("<html>");
			out.println("<head><title>Listado de Data Sources</title></head>");
			out.println("<body>");
			out.println("<h1>Prueba de conexi�n ("+ s_param_jndi + ")</h1>");
			
			out.println("<a href=\"/LibertyTestConnections/\">Volver</a>");
			out.println("<br/><br/>");

			Connection connection = null;
			try {
				InitialContext context = new InitialContext();
				DataSource dataSource = (DataSource) context.lookup(s_param_jndi);
				out.println("<li>Get datasource from jndi " + s_param_jndi + " <strong>Ok!</strong></li>");
				connection = dataSource.getConnection();
				out.println("<li>Get connection from datsource " + s_param_jndi + " <strong>Ok!</strong></li>");

			} catch (NamingException e) {
				out.println("<strong>Error obteniendo la fuente de datos ("+ s_param_jndi +")!!! --> " + e.getMessage()+ "</strong></li><br/><br/>");

				e.printStackTrace(out);
			} catch (SQLException e) {
				out.println("<strong>Error en la conexi�n!!! --> " + e.getMessage()+ "</strong></li><br/><br/>");
				e.printStackTrace(out);
			}
			
			out.println("</body>");
			out.println("</html>");
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	
	/**
	 * @param out
	 */
	private void listDataSources(PrintWriter out){
		try {
			out.println("<html>");
			out.println("<head><title>Listado de Data Sources</title></head>");
			out.println("<body>");
			out.println("<h1>Listado de fuentes de datos:</h1>");
			InitialContext ictx = new InitialContext();
	        Context ctx = (Context) ictx.lookup("");
			//System.out.println("java: = " + ctx.getClass().getName());
			iterateContext(out, ctx);
			out.println("</body>");
			out.println("</html>");
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	
	/**
	 * @param out
	 * @param ctx
	 * @throws ServletException
	 * @throws IOException
	 * @throws NamingException
	 */
	private void iterateContext(PrintWriter out, Context ctx) throws ServletException, IOException, NamingException {
		String s_base_ctx_name = ctx.getNameInNamespace();
		//System.out.println("Namespace: " + s_base_ctx_name);
		if (s_base_ctx_name.compareToIgnoreCase("")!=0){
			s_base_ctx_name = s_base_ctx_name + "/";
		}
		NamingEnumeration<Binding> en = ctx.listBindings("");
		while (en.hasMore()) {
			Binding b = en.next();
			//out.println(new String(tabs) + b.getName() + " = " + b.getClassName());
			try {
				if(b.getClassName().compareTo("com.ibm.ws.rsadapter.jdbc.WSJdbcDataSource")==0){
					String s_ds_jndi = s_base_ctx_name + b.getName();
					out.println("<li>"+s_ds_jndi+" <a href=\"" + getTestConnectionURL(s_ds_jndi) + "\">Test connection</a></li>") ;
					
				}else if (b.getObject() instanceof Context) {
					iterateContext(out, (Context) b.getObject());
				}
			} catch (Exception exc) {
				exc.printStackTrace();
				throw new ServletException(exc);
			}
		}
	}
	
	/**
	 * @param s_ds_jndi
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String getTestConnectionURL (String s_ds_jndi) throws UnsupportedEncodingException{
		String encodedString = URLEncoder.encode(s_ds_jndi, "UTF-8");
		return "/LibertyTestConnections/TestConnection?jndi=" + encodedString;
	}
}
