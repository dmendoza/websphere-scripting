package com.microgestion.test;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class TestDataSourceInjection01
 */
@WebServlet("/TestDataSourceInjection01")
public class TestDataSourceInjection01 extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    @Resource(name="jdbc/derby-test")
    private DataSource myDB_1;
    @Resource(name="jdbc/derby-test")
    private DataSource myDB_2;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestDataSourceInjection01() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
