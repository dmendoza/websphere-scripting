package com.itau.gapw.scripting.liberty;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;
import com.sun.tools.attach.VirtualMachineDescriptor;

public class LibertyDataSourceTest {
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static final String CONNECTOR_ADDRESS = "com.sun.management.jmxremote.localConnectorAddress";
	private static final int CST_APP_RETURN_CODE_SUCCESS = 0;
	private static final int CST_APP_RETURN_CODE_FAIL = -1;
	
	private static final String CST_APP_STATE_STARTED = "STARTED";
	MBeanServerConnection _mbean_serverconnection = null;
	private String _appName;
	private VirtualMachine _vm;
	
	
	/**
	 * @param _vm_desc
	 * @param _appName
	 * @throws AttachNotSupportedException
	 * @throws IOException
	 * @throws AgentLoadException
	 * @throws AgentInitializationException
	 */
	public LibertyDataSourceTest(VirtualMachineDescriptor _vm_desc, String _appName) throws AttachNotSupportedException, IOException, AgentLoadException, AgentInitializationException {
		super();
		String connectorAddress;
		this._appName = _appName;
		this._vm = VirtualMachine.attach(_vm_desc.id());

		// Cadena para inicializar agente y obtener cadena de conexi�n
		String agent = "instrument," + _vm.getSystemProperties().getProperty("java.home") + File.separator + "lib" + File.separator + "management-agent.jar=";
		_vm.loadAgentLibrary(agent);
		connectorAddress = _vm.getSystemProperties().getProperty(CONNECTOR_ADDRESS);
		JMXServiceURL url = new JMXServiceURL(connectorAddress);
		JMXConnector connector = JMXConnectorFactory.connect(url);
		_mbean_serverconnection = connector.getMBeanServerConnection();
	}

	/**
	 * @return
	 * @throws MalformedObjectNameException
	 * @throws IOException
	 * @throws Exception
	 */
	public int getDatasourceTestConnection() throws MalformedObjectNameException, IOException, Exception{
		String sMBeanForApp = "WebSphere:service=com.ibm.websphere.application.ApplicationMBean,name="+ _appName;
		
		String sMBeanForDS= "WebSphere:type=com.ibm.ws.jca.cm.mbean.ConnectionManagerMBean,jndiName=jdbc/exampleDS";
		sMBeanForDS = "WebSphere:type=com.ibm.ws.jca.cm.mbean.ConnectionManagerMBean,jndiName=jdbc/exampleDS,name=dataSource[DB2ExampleDS]/connectionManager[default-0]";
				
		ObjectName connectionMBean = new ObjectName(sMBeanForDS);
		if (_mbean_serverconnection.isRegistered(connectionMBean)) {
			//String state = (String) _mbean_serverconnection.getAttribute(appMBean, "State");	
//			System.out.println("App " + _appName + " reported " + state + " state");
//			if(state.compareTo(CST_APP_STATE_STARTED)==0){
//				return CST_APP_RETURN_CODE_SUCCESS;
//			}else{
//				return CST_APP_RETURN_CODE_FAIL;
//			}
		}else{
			throw new Exception("No se encuentra un MBean para la cadena " + sMBeanForDS);
		}
		return CST_APP_RETURN_CODE_SUCCESS;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		VirtualMachineDescriptor vm_desc = null;
		String serverName;
		CharSequence serverPattern;
		String appName;
		
		// Procesamiento de argumentos
		if (args.length == 2) {
			try {
				serverName = args[0];
				System.out.println("Server: " + serverName);
				if (OS.indexOf("win") >= 0)
					serverPattern = "run " + serverName;
				else 
					serverPattern = "ws-server.jar " + serverName;
				System.out.println("Server pattern: " + serverPattern);
				appName = args[1];
				System.out.println("App: " + appName);
			} catch (Exception e) {
				throw new IllegalArgumentException("Error en los par�metros suministrados (2).");
			}
		}else{
			throw new IllegalArgumentException("Error en los par�metros suministrados (1).");
		}
		
		// Obtenci�n de JVM asociada al nombre del servidor
		List<VirtualMachineDescriptor> vms = VirtualMachine.list();		
		for(int iCountVM = 0; iCountVM < vms.size(); iCountVM++){
			System.out.println("JVM["+iCountVM+"]: " + vms.get(iCountVM).displayName());
			vm_desc = vms.get(iCountVM);
			if(vm_desc.displayName().contains(serverPattern)){
				break;
			}
			vm_desc = null;
		}

		try {
			if (vm_desc != null){
				System.out.println("JVM found: " + vm_desc.displayName());
				LibertyDataSourceTest app = new LibertyDataSourceTest(vm_desc, appName);
				int state = app.getDatasourceTestConnection();
				System.out.println("Exiting with code: " + state);
				System.exit( state );
			}else{
				throw new IllegalArgumentException("No se encontr� ninguna JVM activa asociada al nombre del servidor: " + serverName);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exiting with code: -1");
			System.exit(CST_APP_RETURN_CODE_FAIL);
		}
		
	}
}


