package com.microgestion.test;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class TestDataSourceInjection03
 */
@WebServlet("/TestDataSourceInjection03")
public class TestDataSourceInjection03 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    @Resource(name="jdbc/derby-test")
    private DataSource myDB_5;
    @Resource(name="jdbc/derby-test")
    private DataSource myDB_6;
    @Resource(name="jdbc/derby-test")
    private DataSource myDB_7;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestDataSourceInjection03() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
